import React, { useState } from 'react';
import * as math from 'mathjs';
import Button from './components/Button';

const Calculator = () => {
    const [result, setResult] = useState("");

    const handleClick = (e) => {
        const value = e.target.name;

        switch(value) {
            case '=': {
                try {
                    setResult(math.evaluate(result) + '');
                } catch {
                    setResult("Erreur : expression invalide");
                }
                break;
            }
            case 'c': {
                setResult("");
                break;
            }
            case '+/-': {
                setResult(result.charAt(0) === '-' ? result.slice(1) : '-' + result);
                break;
            }
            default: {
                setResult(result + value);
            }
        }
    }

    return (
        <> 
            <div className='bg-slate-500 p-4 rounded-2xl shadow-3xl font-extrabold'>
                <form className='mb-4'>
                    <input type="text" value={result} className='w-[250px] h-[70px] bg-sky-100 rounded shadow-2xl p-2'/>
                </form>
                <div className='grid grid-cols-4 grid-rows-4 gap-5'>
                    <Button handleClick={handleClick} />
                    <button name='+/-' onClick={handleClick} className='bg-amber-500 rounded'>+/-</button>
                </div >   
            </div>
        </>
    );
}

export default Calculator;