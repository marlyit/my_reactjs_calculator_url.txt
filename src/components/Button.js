// src/components/Button.js
import React from 'react';
import { buttons } from './buttonConf';

const Button = ({ handleClick }) => {
    return (
        <>
            {buttons.map((button, index) => (
                <button key={index} name={button.name} onClick={handleClick} className={button.styleClass}>{button.name}</button>
            ))}
        </>
    );
}

export default Button;